assume cs:code, ds:data, ss:stack

stack segment
	;自定义栈段容量
	db 100 dup(0)

stack ends


; 数据段开始
data segment  
	;data段
	db 100 dup(0)
 
data ends



; 代码段开始
code segment

    ;int 21h   
    mov ax,1122H
    mov bx,3344H
    mov ax,bx

    
    ;正常退出程序，相当于return 0
    ;只要ah是4CH就可以正常结束
    ;a1是返回码
    ;int 10h用于执行BIOS终端
    ;int 3断点中断，用于调试程序
    ;int 21h用于执行DOS系统功能调用，
    ;AH寄存器存储功能号
    mov ah,4CH
    int 21H
    
    ;在汇编语言中用db定义数据
    ;定义一个字节的00H
    db 0H
    ;定义一个字的数据0000H
    dw 0H
    ;在数据段定义数据相当于创建全局变量
    ;在栈段定义数据相当于指定栈的容量
    ;在代码段一般不定义数据
    
    ;批量定义数据
    dw 3 dup(1234H)
    ;声明3个1234H
    
    
    
code ends ;代码段结束


end




